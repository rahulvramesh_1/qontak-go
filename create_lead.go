package qontakgo

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"time"
)

//Qontak - provides the interface for all the operations
//related to Qontak lead
type Qontak struct {
	timeout     time.Duration
	accessToken string
	client      *http.Client
}

//NewQontak creates the new instance
func NewQontak(token string) (*Qontak, error) {

	if token == "" {
		return &Qontak{}, errors.New("please initialize the library with access token")
	}

	//golang http client
	tp := http.Transport{}

	tp.DialContext = (&net.Dialer{
		Timeout: defaultTimeout,
	}).DialContext

	tp.TLSHandshakeTimeout = defaultTimeout
	tp.ResponseHeaderTimeout = defaultTimeout
	tp.ExpectContinueTimeout = defaultTimeout

	// this should be used everytime. no need to create new one for each Get.
	c := http.Client{
		Transport: &tp,
	}

	return &Qontak{client: &c, accessToken: token}, nil
}

//NewLead - add new lead to Qontak
func (q *Qontak) NewLead(payload []byte) (QontakResponse, error) {

	var (
		response QontakResponse
		apiURL   string
	)

	if os.Getenv("QONTAK_MODE") == "PRODUCTION" {
		apiURL = newLeadEndPointProd
	} else {
		apiURL = newLeadEndPointStaging
	}

	req, err := http.NewRequest("POST", apiURL, bytes.NewBuffer(payload))

	if err != nil {
		return QontakResponse{}, err
	}

	req.Header.Add("Authorization", "Basic "+q.accessToken)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("cache-control", "no-cache")

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return QontakResponse{}, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	//check for possible error
	//convert json to struct
	err = json.Unmarshal(body, &response)
	if err != nil {
		return QontakResponse{}, err
	}

	switch res.StatusCode {
	case 422:
		//payload not correct
		log.Printf("error updating data - status code = %d ,error type = %s , error message = %s", response.Meta.Status, response.Meta.Type, response.Meta.DeveloperMessage)
		return response, errors.New(response.Meta.DeveloperMessage)
	case 200:
		log.Println("record updated to Qontak")
		return response, nil
	default:
		log.Printf("error updating data - status code = %d ,error type = %s , error message = %s", response.Meta.Status, response.Meta.Type, response.Meta.DeveloperMessage)
		return response, errors.New(response.Meta.DeveloperMessage)
	}
}
