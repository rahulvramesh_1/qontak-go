package qontakgo

import (
	"time"
)

const (
	//defaultTimeout - timeout second for Qontak connection
	defaultTimeout = time.Second * 10

	//please note , this end point is only for cohive
	newLeadEndPointProd = "https://www.qontak.com/api/evhive/v1/crm/leads/contact"

	//staging url
	newLeadEndPointStaging = "http://staging.qontak.com/api/evhive/v1/crm/leads/contact"
)
